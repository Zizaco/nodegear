<?php

/**
 * Users Controller manages the user creation, auth-
 * -entication, confirmation, login, etc.
 */
class Users_Controller extends Base_Controller
{
    /**
     * RESTful controller
     */
    public $restful = true;

    /**
     * Check user permissions
     */
    public function __construct()
    {
        $this->filter( 'before', 'auth' )->except( array(
            'signup', 'activate', 'login', 'lost_password' 
        ) );
    }

    /**
     * GET signup
     *
     * Shows a form for account creation
     */
    public function get_signup()
    {
        return View::make( 'users.signup' );
    }

    /**
     * POST signup
     *
     * Action of the signup form. It saves the new user
     * if valid, which will trigger the confirmation mail
     * send.
     */
    public function post_signup()
    {
        $input = array(
            'username' => Input::get( 'username' ),
            'email' => Input::get( 'email' ),
            'password' => Input::get( 'password' ),
        );

        $user = User::signup( $input );

        if ( $user->id ) {
            return Redirect::to('general/signup_done');
        }else {
            Input::replace($input);
            return Redirect::to('users/signup')
                ->with_input('except', array('password'))
                ->with_errors($user->errors);
        }
    }

    /**
     * GET login
     *
     * Shows the user login form
     */
    public function get_login()
    {
        if( Auth::check() )
        {
            return Redirect::to('dashboard');
        }else{
            return View::make( 'users.login' );    
        }
    }

    /**
     * POST login
     *
     * Performs the user authentication
     */
    public function post_login()
    {
        $input = array(
            'username' => Input::get( 'email' ),
            'password' => Input::get( 'password' ),
        );

        if ( User::login( $input ) ) {
            return Redirect::to('dashboard');
        }else {
            $err_msg = "Incorrect e-mail or password.";
            return Redirect::to('users/login')
                ->with( 'error', $err_msg );
        }
    }

    /**
     * GET logout
     *
     * Performs the user logout. Redirects to home
     */
    public function get_logout()
    {
        Auth::logout();

        return Redirect::to('/');
    }

    /**
     * GET activate
     *
     * Activate the user with the referred activation
     * key.
     */
    public function get_activate( $activation_key )
    {
        if ( User::activate( $activation_key ) ) {
            return Redirect::to('general/activated');
        }else {
            return Response::error('404');
        }
    }

    /**
     * GET lost_password
     *
     * Should show the lost password form.
     */
    public function get_lost_password()
    {
        return "lost_password";
    }
}
