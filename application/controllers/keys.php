<?php

/**
 * Keys Controller is responsible for the creation and
 * managment of the users shh keys. It interacts direc-
 * -tly with the Key model.
 */
class Keys_Controller extends Base_Controller
{
    /**
     * RESTful controller
     */
    public $restful = true;

    /**
     * Check user permissions
     */
    public function __construct()
    {
        $this->filter( 'before', 'auth' );
    }

    /**
     * GET index
     *
     * Redirects to the dashboard
     */
    public function get_index()
    {
        return Redirect::to( 'dashboard' );
    }

    /**
     * GET new
     *
     * Shows a form for key creation
     */
    public function get_new()
    {
        $user = Auth::user();

        return View::make( 'keys.new' )
            ->with( 'user', $user );
    }

    /**
     * POST create
     *
     * Action of the new form. It saves the user new
     * ssh_key and redirects to dashboard; Or to the
     * GET new with error and input values in case of
     * invalid data.
     */
    public function post_create()
    {
        $user = Auth::user();

        $input = array(
            'name' => Input::get( 'name' ),
            'value' => trim(Input::get( 'value' )),
            'owner_id' => $user->id,
            'status' => 'todo',
        );

        $key = new Key( $input );

        if( $key->save() )
        {
            return Redirect::to('dashboard');
        }
        else
        {
            return Redirect::to('keys/new')
                ->with( 'user', $user )
                ->with_input()
                ->with_errors($key->errors);
        }
    }

    /**
     * GET edit
     *
     * Shows a form for key editing. The fields are
     * filled with the key data.
     */
    public function get_edit( $id )
    {
        $user = Auth::user();

        $key = Key::where( 'id', '=', $id )
            ->where( 'owner_id', '=', $user->id)
            ->first();

        if($key == null)
        {
            return Response::error('404');
        }
        else
        {
            return View::make( 'keys.edit' )
                ->with( 'user', $user )
                ->with( 'key', $key );
        }
    }

    /**
     * PUT update
     *
     * Action of the edit form. It saves the changes
     * and redirect to dashboard; Or redirects to
     * GET edit with inpur and errors.
     */
    public function put_update( $id )
    {
        $user = Auth::user();

        $key = Key::where( 'id', '=', $id )
            ->where( 'owner_id', '=', $user->id)
            ->first();

        if($key == null)
        {
            return Response::error('404');
        }

        $key->name = Input::get( 'name' );
        $key->value = trim(Input::get( 'value' ));
        // Set the status to todo. This way the artisan
        // script to build the key_file will run on any
        // change to the key
        $key->status = 'todo';

        if( $key->save() )
        {
            return Redirect::to('dashboard');
        }
        else
        {
            //dd($key->errors);
            return Redirect::to('keys/edit/'.$id)
                ->with( 'user', $user )
                ->with_input()
                ->with_errors($key->errors);
        }
    }
}
