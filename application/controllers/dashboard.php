<?php

/**
 * The dashboard is the main screen for an authentic-
 * -cated user.
 * this place show the repositories and the ssh keys
 * of the current_user.
 */
class Dashboard_Controller extends Base_Controller
{
    /**
     * RESTful controller
     */
    public $restful = true;

    /**
     * Check user permissions
     */
    public function __construct()
    {
        $this->filter( 'before', 'auth' );
    }

    /**
     * GET index
     *
     * Shows the user dashboard.
     */
    public function get_index()
    {
        $user = Auth::user();
        $repositories = $user->repositories()->
                            order_by('id','desc')->get();

        return View::make( 'dashboard.index' )
            ->with( 'user', $user )
            ->with( 'repositories', $repositories );
    }
}
