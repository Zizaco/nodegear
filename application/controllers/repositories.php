<?php

/**
 * Repositories Controller is responsible for the creation
 * and managment of user repositories.
 */
class Repositories_Controller extends Base_Controller
{
    /**
     * RESTful controller
     */
    public $restful = true;

    /**
     * Check user permissions
     */
    public function __construct()
    {
        $this->filter( 'before', 'auth');
    }

    /**
     * GET index
     *
     * Redirects to the dashboard
     */
    public function get_index()
    {
        return Redirect::to( 'dashboard' );
    }

    /**
     * GET show
     *
     * Shows the information of a repository
     */
    public function get_show( $id )
    {
        $user = Auth::user();

        $repo = Repository::where( 'id', '=', $id )
            ->where( 'owner_id', '=', $user->id)
            ->first();

        if($repo == null)
        {
            return Response::error('404');
        }
        else
        {
            return View::make( 'repositories.show' )
                ->with( 'user', $user )
                ->with( 'repo', $repo );
        }
    }

    /**
     * GET new
     *
     * Shows a form for repository creation
     */
    public function get_new()
    {
        $user = Auth::user();

        return View::make( 'repositories.new' )
            ->with( 'user', $user );
    }

    /**
     * POST create
     *
     * Action of the new form. It saves the repository
     * and redirects to dashboard; Or to the GET new
     * with error and input values in case of invalid data.
     */
    public function post_create()
    {
        $user = Auth::user();

        $input = array(
            'name' => Input::get( 'name' ),
            'description' => Input::get( 'description' ),
            'owner_id' => $user->id,
            'status' => 'todo',
        );

        $repo = new Repository( $input );

        if( $repo->save() )
        {
            return Redirect::to('dashboard');
        }
        else
        {
            return Redirect::to('repositories/new')
                ->with( 'user', $user )
                ->with_input()
                ->with_errors($repo->errors);
        }
    }
}
