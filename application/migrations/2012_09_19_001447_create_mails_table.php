<?php

class Create_Mails_Table {

	public function up()
	{
		Schema::create('mails', function($table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('subject');
			$table->text('message');
			$table->integer('tries')->default(0);
			$table->boolean('sent')->default(false);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('mails');
	}

}
