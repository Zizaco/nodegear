<?php

class Create_Users_Table {

	public function up()
	{
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email',100)->unique();
			$table->string('password',60);
			$table->string('confirmation_key',60);
			$table->boolean('active',60)->default(false);
			$table->timestamps();
		});

		User::create(
			array(
				'username' => 'zizaco',
				'email' => 'zizaco@gmail.com',
				'password' => Hash::make('63366336'),
				'confirmation_key' => substr(md5(date('U')),0,10),
				'active' => true
			)
		);
	}

	public function down()
	{
		Schema::drop('users');
	}

}
