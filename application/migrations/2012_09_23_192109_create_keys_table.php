<?php

class Create_Keys_Table {

	public function up()
	{
		Schema::create('keys', function($table) {
			$table->increments('id');
			$table->integer('owner_id');
			$table->string('name');
			$table->text('value');
			$table->string('status')->default('todo');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('keys');
	}

}
