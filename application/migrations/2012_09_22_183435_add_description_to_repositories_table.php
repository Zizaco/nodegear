<?php

class Add_Description_To_Repositories_Table {

	public function up()
	{
		Schema::table('repositories', function($table) 
		{
			$table->text('description');

		});
	}

	public function down()
	{
		Schema::table('repositories', function($table) {
			$table->drop_column('description');
		});
	}

}
