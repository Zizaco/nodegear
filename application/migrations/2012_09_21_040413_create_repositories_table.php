<?php

class Create_Repositories_Table {

	public function up()
	{
		Schema::create('repositories', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('owner_id');
			$table->string('status')->default('todo');
			$table->timestamps();
		});

		Repository::create(
			array(
				'name' => 'test_repo',
				'owner_id' => User::min('id'),
				'status' => 'todo'
			)
		);
	}

	public function down()
	{
		Schema::drop('repositories');
	}

}
