<?php

class TestDashboardController extends Base_Test {
    /**
     * Should run migrations before run tests
     */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /**
     * Clean table between every test
     */
    public function setUp()
    {
        DB::table('repositories')->delete();
    }

    /**
     * Shall get dashboard index
     */
    public function testShouldGetIndex()
    {
        $user = FactoryMuff::create( 'User' );

        // Login
        Auth::login($user);

        // Do request
        $result = $this->http_request('GET','dashboard');

        // expects a 200 answer
        $this->assertEquals(200, $result->status());
    }

    /**
     * Permissions
     */
    public function testPermissions()
    {
        // Not get index if not logged
        $this->assertRequestResponse('GET','dashboard', 500);
    }
}
