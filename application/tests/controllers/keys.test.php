<?php

class TestKeysController extends Base_Test {
    /**
     * Should run migrations before run tests
     */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /**
     * Clean table between every test
     */
    public function setUp()
    {
        DB::table( 'keys' )->delete();
    }

    /**
     * Should get new
     */
    public function testShouldGetNew()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        $result = $this
            ->http_request('GET','keys/new');

        // Expects a success
        $this->assertEquals(200, $result->status());
    }

    /**
     * Should post create
     */
    public function testShouldPostCreate()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        $params = FactoryMuff::attributes_for( 'Key' );

        // simulates a post input
        Input::replace($params);
        $result = $this
            ->http_request('POST','keys/create');

        // Expects a success
        $this->assertRedirect($result,'dashboard');
    }

    /**
     * Should get edit
     */
    public function testShouldGetEdit()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        // A previosly created key
        $key = FactoryMuff::create(
        'Key', array(
           'owner_id' => $user->id,
        ));

        $result = $this
            ->http_request('GET','keys/edit/'.$key->id);

        // Expects a success
        $this->assertEquals(200, $result->status());
    }

    /**
     * Should post create
     */
    public function testShouldPutUpdate()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        // A previosly created key
        $key = FactoryMuff::create(
        'Key', array(
           'owner_id' => $user->id,
        ));

        $params = FactoryMuff::attributes_for( 'Key' );

        // simulates a post input
        Input::replace($params);
        $result = $this
            ->http_request('PUT','keys/update/'.$key->id);

        // Expects a success
        $this->assertRedirect($result,'dashboard');
    }

    /**
     * Permissions
     */
    public function testPermissions()
    {
        // Should not get new if Not logged
        $this->assertRequestResponse('GET','keys/new', 500);

        // Should not post create if Not logged
        $this->assertRequestResponse('POST','keys/create', 500);

        // Should not get edit if Not logged
        $key = FactoryMuff::create( 'Key' );
        $this->assertRequestResponse('GET','keys/edit'.$key->id, 500);

        // Should not put update if Not logged
        $this->assertRequestResponse('PUT','keys/update'.$key->id, 500);

        // Should not get edit if Not owner
        Auth::login( 
            FactoryMuff::create( 'User', array( 'active' => true ) ) 
        );
        $this->assertRequestResponse('GET','keys/edit'.$key->id, 404);

        // Should not put update if Not owner
        $this->assertRequestResponse('PUT','keys/update'.$key->id, 404);
    }
}
