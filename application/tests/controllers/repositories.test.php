<?php

class TestRepositoriesController extends Base_Test {
    /**
     * Should run migrations before run tests
     */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /**
     * Clean table between every test
     */
    public function setUp()
    {
        DB::table( 'repositories' )->delete();
    }

    /**
     * Shall show repository if owner
     */
    public function testShouldShow()
    {
        $repo = FactoryMuff::create( 'Repository' );
        $user = $repo->owner;

        Auth::login( $user );

        $result = $this
            ->http_request('GET','repositories/show/'.$repo->id);

        // Expects a success
        $this->assertEquals(200, $result->status());
    }

    /**
     * Should get new if logged
     */
    public function testShouldGetNew()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        $result = $this
            ->http_request('GET','repositories/new');

        // Expects a success
        $this->assertEquals(200, $result->status());
    }

    /**
     * Should post create if logged
     */
    public function testShouldPostCreate()
    {
        $user = FactoryMuff::create( 'User' );
        Auth::login( $user );

        $params = FactoryMuff::attributes_for( 'Repository' );

        // simulates a post input
        Input::replace($params);
        $result = $this
            ->http_request('POST','repositories/create');

        // Expects a success
        $this->assertRedirect($result,'dashboard');
    }

    /**
     * Permissions
     */
    public function testPermissions()
    {
        // Should not get new if Not logged
        $this->assertRequestResponse('GET','repositories/new', 500);

        // Should not post create if Not logged
        $this->assertRequestResponse('POST','repositories/create', 500);

        // Shall not show repository if not logged
        $repo = FactoryMuff::create( 'Repository' );
        $this->assertRequestResponse('GET','repositories/show/'.$repo->id, 500);

        // Shall not show repository if not owner
        Auth::login( 
            FactoryMuff::create( 'User', array( 'active' => true ) ) 
        );
        $this->assertRequestResponse('GET','repositories/show/'.$repo->id, 404);
    }
}
