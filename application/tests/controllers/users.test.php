<?php

class TestUsersController extends Base_Test {

	/*
	 Should run migrations before run tests
	*/
	public static function setUpBeforeClass()
	{
		static::migrate();
		static::use_sessions();
	}

	/*
	 Clean table between every test
	*/
	public function setUp()
	{
		DB::table('users')->delete();
	}

	/*
	 User shall signup
	*/
	public function testGetSignup()
	{
		$result = $this->http_request('GET','users/signup');

		// expects a 200 answer
        $this->assertEquals(200, $result->status());
	}

	/*
	 User shall signup
	*/
	public function testPostSignup()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'email' => 'example@gmail.com',
			'password' => 'secret',
		));

		$user = new User($params);

		// simulates a post input
		Input::replace($params);
		$result = $this->http_request('POST','users/signup');

		// expects to be redirected to
		$this->assertRedirect($result,'general/signup_done');
	}

	/*
	 User shall activate account
	*/
	public function testGetActivate()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'email' => 'example@gmail.com',
			'password' => 'secret',
		));

		// create user
		$user = User::signup($params);
		$this->assertTrue($user->id != null);

		// access activation url
		$activation_url = 'users/activate/'.$user->confirmation_key;
		$result = $this->http_request('GET',$activation_url);

		// expects to be redirected to
		$this->assertRedirect($result,'general/activated');
	}

	/*
	 User shall login
	*/
	public function testGetLogin()
	{
        // Fails miserably
        $this->assertFalse(Auth::check());

		$result = $this->http_request('GET','users/login');

		// expects a 200 answer
        $this->assertEquals(200, $result->status());
	}

	/*
	 User shall signup
	*/
	public function testPostLogin()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'email' => 'example@gmail.com',
			'password' => 'secret',
		));

		$user = User::signup($params);

		// Activate the new user
		User::activate( $user->confirmation_key );

		// simulates a post input
		Input::replace($params);
		$result = $this->http_request('POST','users/login');

		// expects to be redirected to
		$this->assertRedirect($result,'dashboard');
	}

	/*
	 User shall logout
	*/
	public function testGetLogout()
	{
		// Create user
		$params = FactoryMuff::attributes_for( 'User' );
		$user = User::signup($params);

		// Login
		Auth::login($user);

		// Send logout request
		$result = $this->http_request('GET','users/logout');

		// expects to be redirected to
        $this->assertEquals(302, $result->status());
	}
}
