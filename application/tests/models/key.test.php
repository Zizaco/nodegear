<?php

class TestKeyModel extends Base_test {
    /**
     * Should run migrations before run tests
     */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /**
     * Clean table between every test
     */
    public function setUp()
    {
        DB::table( 'keys' )->delete();
    }

    /**
     * Should save valid key
     */
    public function testShouldSaveValidKey()
    {
        $params = FactoryMuff::attributes_for( 'Key' );

        $key = new Key( $params );
        $this->assertTrue( $key->save() );
    }

    /**
     * Should not save invalid key
     */
    public function testShouldNotSaveInvalidKey()
    {
        $params = FactoryMuff::attributes_for(
            'Key', array(
            'name' => '&?_/',
        ));

        $key = new Key( $params );
        $this->assertFalse( $key->save() );
    }
}
