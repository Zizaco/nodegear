<?php

class TestUserModel extends Base_Test {

	/*
	 Should run migrations before run tests
	*/
	public static function setUpBeforeClass()
	{
		static::migrate();
		static::use_sessions();
	}

	/*
	 Clean table between every test
	*/
	public function setUp()
	{
		DB::table('users')->delete();
	}

	/*
	 Should signup ACTIVATE and login
	*/
	public function testUserShouldSignupActivateAndLogin()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'password' => 'secret',
			'active' => false,
		));

		$user = User::signup($params);
		$this->assertTrue($user->id != null);

		// Activate the new user
		User::activate( $user->confirmation_key );

		// should login
		$credentials = array(
			'username' => $user->email,
			'password' => 'secret',
		);
		$this->assertTrue(User::login($credentials));
	}

	/*
	 Should NOT login non active user
	*/
	public function testUserShouldNotLoginNonActive()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'email' => 'example@gmail.com',
			'password' => 'secret',
			'active' => false,
		));

		$this->assertTrue(User::signup($params)->id != null);

		// Should not login non active
		$credentials = array(
			'username' => 'example@gmail.com',
			'password' => 'secret',
		);
		
		$this->assertFalse(User::login($credentials));
	}

	/*
	 Signup should create confirmation mail
	*/
	public function testUserSignupShouldCreateMail()
	{
		$params = FactoryMuff::attributes_for( 'User' );

		$user = User::signup($params);
		$this->assertTrue($user->id != null);

		$signup_mail_count = count($user->mails()->get());

		$this->assertTrue($signup_mail_count == 1);
	}

	/*
	 Should not signup uncomplete
	*/
	public function testUserShouldNotSignupUncomplete()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
			'email' => '',
		));

		$this->assertFalse(User::signup($params)->id != null);
	}

	/*
	 Should not signup duplicate
	*/
	public function testUserShouldNotSignupDuplicate()
	{
		$params = FactoryMuff::attributes_for(
			'User', array(
		));

		// first record
		User::signup($params);
		// second with same data, should fail
		$this->assertFalse(User::signup($params)->id != null);
	}

	/*
	 Should reset password
	*/
	public function testUserShouldResetPassword()
	{
		$old_password = 'secret';

		$params = FactoryMuff::attributes_for(
			'User', array(
			'password' => $old_password,
			'active' => false,
		));

		$user = User::signup($params);

		// Activate and reset password
		User::activate( $user->confirmation_key );
		$new_password = $user->reset_password();

		// new password should be different from the old one
		$this->assertNotEquals($new_password, $old_password);

		// should login with new password
		$credentials = array(
			'username' => $user->email,
			'password' => $new_password,
		);
		$this->assertTrue(User::login($credentials));
	}
}
