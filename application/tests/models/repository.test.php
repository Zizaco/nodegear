<?php

use NodeGear\GitOperations;

class TestRepositoryModel extends Base_test {

    /**
     * Should run migrations before run tests
     */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /**
     * Clean table between every test
     */
    public function setUp()
    {
        DB::table( 'repositories' )->delete();
    }

    /**
     * Should return info
     */
    public function testShouldReturnInfo()
    {
        $repo = FactoryMuff::create( 'Repository' );

        // Repository name
        $full_name = $repo->owner->username."/".$repo->name;
        $this->assertEquals( $repo->full_name(), $full_name );

        // Repository path
        $path = GitOperations::$repo_path.$repo->full_name().".git";
        $this->assertEquals( $repo->path(), $path );
    }

    /**
     * Should save a valid repository
     */
    public function testShouldSaveValidRepository()
    {
        $params = FactoryMuff::attributes_for( 'Repository' );

        $repo = new Repository( $params );
        $this->assertTrue( $repo->save() );
    }

    /**
     * Should not save invalid repository
     */
    public function testShouldNotSaveInvalidRepository()
    {
        $params = FactoryMuff::attributes_for(
            'Repository', array(
            'name' => '&?_/',
        ));

        $this->assertFalse( Repository::    create( $params ) );
    }
}
