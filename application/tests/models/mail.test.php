<?php

class TestMailModel extends Base_Test {

    /*
     Should run migrations before run tests
    */
    public static function setUpBeforeClass()
    {
        static::migrate();
        static::use_sessions();
    }

    /*
     Clean table between every test
    */
    public function setUp()
    {
        DB::table( 'users' )->delete();
    }

    /*
     Should send mail
    */
    public function testShouldSendMail()
    {
        $params = FactoryMuff::attributes_for( 'Mail' );

        $mail = new Mail( $params );
        $this->assertTrue( $mail->save() );

        // send e-mail
        $this->assertTrue( $mail->send() );
    }
}
