<?php

abstract class Base_Test extends PHPUnit_Framework_TestCase
{
    /**
     * Prepare environment
     */
    public function __construct()
    {
        // Load bundles that may be used in tests
        Bundle::start( 'composer' );
        Bundle::start( 'factorymuff' );
    }

    /**
     * TearDown after each test
     */
    public function tearDown()
    {
        // Logout in this brutal way is needed
        // since the instance of the user used
        // to login in the last test is probably
        // gone. So just calling Auth::logout()
        // don't works.
        Auth::login(-1); Auth::logout();
    }

    /**
     * Run the migrations in the test database
     */
    public static function migrate()
    {
        // If there is not a declaration that migrations have been run'd
        if ( !isset( $GLOBALS['migrated_test_database'] ) ) {
            // Run migrations
            require path( 'sys' ).'cli/dependencies'.EXT;
            Laravel\CLI\Command::run( array( 'migrate:install' ) );
            Laravel\CLI\Command::run( array( 'migrate' ) );
            // Declare that migrations have been run'd
            $GLOBALS['migrated_test_database'] = true;
        }
    }
    /**
     * Enable sessions to be used in tests. For
     * authentication purposes.
     */
    public static function use_sessions()
    {
        Session::started() or \Session::load();
    }

    /**
     * Simulates a request to the router re-setting
     * the Method
     */
    public function http_request( $method, $route )
    {
        $request = \Router::route( $method, $route );
        Request::setMethod( $method );
        if ( $request == null ) {
            $this->fail( 'Invalid route: '.$route );
        }
        return $request->call();
    }

    /**
     * Checks if the request response is an correct
     * redirect
     */
    public function assertRedirect( $result, $url )
    {
        $this->assertEquals( 302, $result->status() );

        $redirected_to =
            substr( $result->headers()->get( 'location' ), 9 );

        $this->assertEquals( $url, $redirected_to );
    }

    /**
     * Checks if the request response is the specified
     */
    public function assertRequestResponse( $method, $route, $response )
    {
        $result = $this->http_request( $method, $route );
        $this->assertEquals($response, $result->status(), 'assertRequestResponse failed');
    }
}
