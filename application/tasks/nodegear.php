<?php

/**
 * Tasks directly related to the application objective:
 * Manage repositories, permissions (ssh_keys) and
 * collaborators.
 */
class NodeGear_Task {

    /**
     * Run method without action should
     * do nothing
     */
    public function run()
    {
        echo "No action specified";
    }

    /**
     * nodegear:repository should init any "todo"
     * repository.
     */
    public function repository()
    {
        // Grab the non sent emails that not failed for 3 times
        $pendent = Repository::order_by('id','desc')
            ->where('status', '=', 'todo')
            ->get();

        echo "\n";

        // Send each e-mail and output info
        foreach ($pendent as $repo) {
            echo "#Initing repository:\n";

            if( $repo->init() )
            {
                echo "  [SUCESS]";
            }
            else
            {
                echo "  [FAILED]";
            }

            echo "\n\n";
        }
    }

    /**
     * nodegear:ssh_key should refresh the ssh_keys
     */
    public function ssh_key()
    {
        // Grab the non sent emails that not failed for 3 times
        $pendent = Key::order_by('id','desc')
            ->where('status', '=', 'todo')
            ->get();

        echo "\n";

        // Send each e-mail and output info
        foreach ($pendent as $key) {
            echo "#Initing ssh_key:\n";

            if( $key->create_key_file() )
            {
                echo "  [SUCESS]";
            }
            else
            {
                echo "  [FAILED]";
            }

            echo "\n\n";
        }
    }
}
