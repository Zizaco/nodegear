<?php

class Deliver_Task {

    /**
     * Deliveries the Mails (model)
     */
    public function run()
    {
        // Grab the non sent emails that not failed for 3 times
        $pendent = Mail::order_by('id','desc')
            ->where('sent', '=', false)
            ->where('tries', '<', 3)
            ->get();

        echo "\n";

        // Send each e-mail and output info
        foreach ($pendent as $mail) {
            echo "#Sending email:\n";
            echo " ".$mail->subject."\n";
            echo " To:".$mail->user->username."\n";
            echo "    ".$mail->user->email."\n";

            if( $mail->send() )
            {
                echo "  [SUCESS]";
            }
            else
            {
                echo "  [FAILED $mail->tries]";
            }

            echo "\n\n";
        }
    }
}
