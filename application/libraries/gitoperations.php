<?php namespace NodeGear;

class GitOperations
{
    // Place the run the git init
    public static $repo_path = "/home/git/repositories/";

    // Gitosis permissions file
    private static $gitosis = "/home/git/gitosis-admin/";

    // Gitosis bare directory
    private static $gitosis_bare = "/home/git/repositories/gitosis-admin.git/";

    // Gitosis ssh_keys directory
    private static $gitosis_keys = "/home/git/gitosis-admin/keydir/";

    /**
     * Refresh the key file for a user adding
     * all the keys that he have.
     */
    public static function refresh_keys( $user )
    {
        // Gitosis admin directory
        $gitosis = "/home/git/gitosis-admin/";

        // Gitosis ssh_keys directory
        $gitosis_keys = "/home/git/gitosis-admin/keydir/";

        // Should be performed only in production
        if(\Laravel\Request::env() != 'prod')
        {
            return true;
        }

        try{
            $user_key_file = $gitosis_keys.$user->email.".pub";

            // Remove actual key file
            echo exec('rm -r -f '.$user_key_file);

            // File content
            $key_file_content = "";

            // Grab all the keys that the user posses
            $keys = $user->keys()->get();

            foreach ($keys as $key) {
                // Take the section that matters at ssh
                $value = explode(" ",$key->value);

                if(isset($value[1]))
                {
                    $value = $value[1];
                }
                else
                {
                    $value = 'invalid_key_format';
                }
                
                // Add to file
                $key_file_content .= "ssh-rsa ".$value." ".$user->email."\n";
            }

            // Save file
            echo exec('echo "'.$key_file_content.'" >  '.$user_key_file);

            // set key to writable
            echo exec('chmod -R 777 '.$user_key_file);

            // push changes
            static::push_gitosis();

            return true;
        }
        catch(Exception $e)
        {
            echo "Error at ssh_key file creation:\n";
            echo $e."\n";

            return false;
        }
    }

    /**
     * Perform all needed operations (in
     * gitosis and in sistem) to make a
     * new repository available to the user
     */
    public static function init_repo( $repo )
    {
        // Should be performed only in production
        if(\Laravel\Request::env() != 'prod')
        {
            return true;
        }

        try{
            // user directory at repos
            $user_path =
                static::$repo_path.
                $repo->owner->username;

            // the new repo path
            $repository_path =
                static::$repo_path.
                $repo->owner->username.'/'.
                $repo->name.".git";

            // if first repo of user create his path
            echo exec("mkdir ".$user_path);
            if(!file_exists($user_path))
            {
                throw new Exception("can't create $user_path");
            }

            // create repo path
            echo exec("mkdir ".$repository_path);
            if(!file_exists($repository_path))
            {
                throw new Exception("can't create $repository_path");
            }

            // create the bare repo
            echo exec('git init '.$repository_path.' --bare');

            // set repo to writable
            echo exec('chmod -R 777 '.$user_path);

            // push changes
            static::refresh_repo_table();

            return true;
        }
        catch(Exception $e)
        {
            echo "Error at repository preparation in gitosis:\n";
            echo $e."\n";

            return false;
        }
    }

    private static function refresh_repo_table()
    {
        $repositories = \Repository::with('owner')->get();

        // File content
        $repo_file_content = "";

        // Base
        $repo_file_content = 
            "[gitosis]\n".
            "\n".
            "[group gitosis-admin]\n".
            "members = zizaco@gmail.com zizaco@nodegear.org\n".
            "writable = gitosis-admin\n".
            "\n".
            "[group nodegear]\n".
            "writable = nodegear\n".
            "members = zizaco@gmail.com\n".
            "\n";

        foreach ($repositories as $repo) {
            $repo_file_content .=
                "\n".
                '[group '.$repo->owner->username.'_'.$repo->name."]\n".
                'writable = '.$repo->owner->username.'/'.$repo->name."\n".
                'members = '.$repo->owner->email."\n";
        }

        // Remove actual repositories file
        echo exec('rm -r -f '.static::$gitosis.'gitosis.conf');

        // Save file
        echo exec('echo "'.$repo_file_content.'" >  '.static::$gitosis.'gitosis.conf');

        // set key to writable
        echo exec('chmod -R 777 '.static::$gitosis.'gitosis.conf');

        // push changes
        static::push_gitosis();

        return true;
    }

    private static function push_gitosis()
    {
        echo exec('git --git-dir='.static::$gitosis.'.git --work-tree='.static::$gitosis.' pull');
        echo exec('git --git-dir='.static::$gitosis.'.git --work-tree='.static::$gitosis.' add . -A');
        echo exec('git --git-dir='.static::$gitosis.'.git --work-tree='.static::$gitosis.' commit -m "updating user keys"');
        echo exec('git --git-dir='.static::$gitosis.'.git --work-tree='.static::$gitosis.' push');
    }
}
