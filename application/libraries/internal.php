<?php namespace NodeGear;

class Internal
{
    private static $force_async =
        " > /dev/null 2>/dev/null &";

    public static function exec_async( $command )
    {
        exec($command.static::$force_async);
    }

    public static function get_dir_size( $path )
    {
        $size_and_path = explode( '/', exec( "du -sh ".$path ) );
        $size = trim( $size_and_path[0] );
        return $size;
    }
}
