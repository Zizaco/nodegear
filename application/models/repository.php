<?php

use NodeGear\Internal;
use NodeGear\GitOperations;

/**
 * Repository model represents an actual git
 * repository within the server. And user may have
 * many repositories and a repository may have a
 * number of collaborators
 */
class Repository extends Aware 
{
    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required|alpha_dash|max:15|unique:repositories',
        'owner_id' => 'numeric',
        'status' => 'required|in:todo,error,fine',
    );

    /**
      * Attribute kind for FactoryMuff
     */
    public static $factory = array(
        'name' => 'string',
        'owner_id' => 'factory|User',
        'status' => 'todo',
        'description' => 'text',
    );

    /**
     * Eager loads the repo owner
     */
    public $includes = array('owner');

    /**
     * Relationship with user
     */
    public function owner()
    {
        return $this->belongs_to('User');
    }

    /**
     * Returns repository full name
     */
    public function full_name()
    {
        return $this->owner->username."/".$this->name;
    }

    /**
     * Gets the repository full path within the
     * server
     */
    public function path()
    {
        return GitOperations::$repo_path.$this->full_name().".git";
    }

    /**
     * Returns the repository size.
     */
    public function size()
    {
        return Internal::get_dir_size($this->path());
    }

    /**
     * Returns an state representation of the repository
     * it may be used as an css class to display the repo
     */
    public function status_string()
    {
        switch ($this->status) {
            case 'fine':
                return 'success';
                break;

            case 'todo':
                return 'warning';
                break;
            
            default:
                return 'error';
                break;
        }
    }

    /**
     * Overwrites the save method to perform an async init
     */
    public function save($rules=array(), $messages=array(), $onSave=null)
    {
        $result = parent::save($rules, $messages, $onSave);

        if( $result )
        {
            // Run Asynchronous
            $command = "php artisan nodegear:repository --env=".Request::env();
            Internal::exec_async( $command );
        }

        return $result;
    }

    /**
     * This method is triggered by the artisan nodegear:
     * repository task. It prepare the bare repository within
     * the gitosis.
     */
    public function init()
    {
        // Update repository status
        $result = GitOperations::init_repo( $this );
        if ( $result )
        {
            // Mark as fine
            $this->status = "fine";
        }
        else
        {
            // Mask as crashed and send an e-mail
            // to admin. Cause this is serious!
            $this->status = "error";

            $crash_report = new Mail( array(
                'user_id' => User::min('id'),
                'subject' => 'Crash Report',
                'message' => View::make('emails.repo_error')
                    ->with('repository', $this)->render(),
            ));

            $crash_report->save();
        }

        return $this->save();
    }
}
