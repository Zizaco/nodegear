<?php

use NodeGear\Internal;

/**
 * The Mail model represents the sent or "should
 * send" mail notifications. And email have an sent
 * attribute which is the status of it.
 */
class Mail extends Eloquent
{
    /**
     * Validation rules
     */
    public static $rules = array(
        'user_id'  => 'required',
        'subject'  => 'required',
        'message'  => 'required',
    );

    /**
     * Attribute kind for FactoryMuff
     */
    public static $factory = array(
        'user_id' => 'factory|User',
        'subject' => 'email',
        'message' => 'text',
    );


    /*
     Relashionship with user
    */
    public function user()
    {
        return $this->belongs_to('User');
    }

    /**
     * Overwrites the save method to run the deliver
     * artisan task after saving an e-mail.
     */
    public function save()
    {
        $result = parent::save();

        if( $result )
        {
            // Run Asynchronous
            $command = "php artisan deliver --env=".Request::env();
            Internal::exec_async( $command );
        }

        return $result;
    }

    /**
     * Triggered by artisan deliver task this method
     * will use swiftmailer actually send the e-mail
     * the user.
     */
    public function send()
    {
        $result = false;

        if( $this->tries < 3 )
        {
            // Send the email
            $result = $this->swiftmailer_send();
            if ( $result )
            {
                // Mark as sent
                $this->sent = true;
            }
            else
            {
                // Count tries
                $this->tries = $this->tries+1;
            }

            $this->save();
        }

        return $result;
    }

    /**
     * The swiftmailer procedures needed in order to deliver
     * an e-mail.
     */
    private function swiftmailer_send()
    {
        // Returns true if running in test environment
        if(Request::env() == 'test')
        {
            return true;
        }

        // Load composer (which will include swiftmailer)
        Bundle::start( 'composer' );

        // Create the message
        $mail = Swift_Message::newInstance();
        $mail->setSubject( $this->subject );
        $mail->setFrom( array( 'zizaco@nodegear.org' => 'NodeGear' ) );
        $mail->setTo( array( $this->user->email ) );
        $mail->setBody( $this->message, 'text/html' );

        // Create the transport and mailer
        $transport = Swift_MailTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);

        // Send the message
        return $mailer->send($mail);
    }
}
