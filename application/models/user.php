<?php

/**
 * User Model represents any user of the application
 * they may signup and confirm the e-mail address
 * before login. An user may have repositories and
 * ssh_keys.
 */
class User extends Aware
{
    /**
     * Validation rules
     */
    public static $rules = array(
        'username'  => 'required|alpha|min:4|max:12|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:4',
    );

    /**
     * Attribute kind for FactoryMuff
     */
    public static $factory = array(
        'username' => 'string',
        'email' => 'email',
        'password' => 'secret',
        'active' => true,
    );

    /**
     * Eager loads stuff that will be used
     * mostly
     */
    public $includes = array('keys');

    /**
     * Relationship with mail
     */
    public function mails()
    {
        return $this->has_many( 'Mail' );
    }

    /**
     * Relationship with repositories
     */
    public function repositories()
    {
        return $this->has_many( 'Repository', 'owner_id' );
    }

    /**
     * Relationship with keys
     */
    public function keys()
    {
        return $this->has_many( 'Key', 'owner_id' );
    }

    /**
     * Resets the password of an user. The new password
     * is returned.
     */
    public function reset_password()
    {
        // generates a random 7 digits string based on time
        $new_password = substr( md5( date( 'U' ) ), 0, 7 );
        $this->password = Hash::make( $new_password );
        $this->save();

        return $new_password;
    }

    /**
     * Static method that activate the user account with
     * the informed confirmation_key.
     */
    public static function activate( $key )
    {
        $user = User::where('confirmation_key', '=', $key)
            ->first();

        if( $user )
        {
            $user->active = true;
            return $user->save();
        }
        else
        {
            return false;
        }
    }

    /**
     * Authenticates user if he confirmed his e-mail (
     * is active) and if his credentials are right.
     */
    public static function login( $credentials )
    {
        $user = User::where('email', '=', $credentials['username'])
            ->first();

        if($user && $user->active)
        {
            return Auth::attempt($credentials);
        }
        else
        {
            return false;
        }
    }

    /**
     * Creates a new user and an confirmation e-mail
     * for him.
     */
    public static function signup( $params_array )
    {
        $new_user = new User( $params_array );

        // "Destroy" passwords smaller then 4 characters long
        if(strlen($new_user->password) >= 4)
        {
            $new_user->password = Hash::make( $new_user->password );
        }
        else
        {
            $new_user->password = "1";   
        }

        // generates a random readable 10 characters key
        $new_user->confirmation_key = substr(md5(date('U')),0,10);

        if( $new_user->save() )
        {
            // generates the confirmation e-mail
            $confirmation = new Mail( array(
                'user_id' => $new_user->id,
                'subject' => 'Confirmation',
                'message' => View::make('emails.signup')
                    ->with('user', $new_user)->render(),
            ));

            $confirmation->save();

            $new_user;
        }

        return $new_user;
    }
}
