<?php

use NodeGear\Internal;
use NodeGear\GitOperations;

/**
 * Key Model represents the SSH keys that an user
 * may have to connect to his repositories. The
 * name attribute is just for identification, what
 * really matters is the value attribute.
 */
class Key extends Aware
{
    /**
     * Validation rules
     */
    public static $rules = array(
        'owner_id' => 'numeric',
        'name' => 'required|alpha_dash|min:3|max:13',
        'value' => 'required',
        'status' => 'required|in:todo,error,fine',
    );

    /**
     * Attribute kind for FactoryMuff
     */
    public static $factory = array(
        'owner_id' => 'factory|User',
        'name' => 'string',
        'value' => 'text',
        'status' => 'todo',
    );

    /**
     * Eager loads the user. Cause almost always
     * the key are gonne be used with user email
     */
    public $includes = array('owner');

    /**
     * Relationship with user
     */
    public function owner()
    {
        return $this->belongs_to('User');
    }

    /**
     * Overwrites the save method to create the
     * SSH file in the keys directory.
     */
    public function save($rules=array(), $messages=array(), $onSave=null)
    {
        // Runs the Aware original save method
        $result = parent::save($rules, $messages, $onSave);

        if( $result )
        {
            // Create the SSH key file asynchronous
            $command = "php artisan nodegear:ssh_key --env=".Request::env();
            Internal::exec_async( $command );
        }

        return $result;
    }

    /**
     * This method is runned asynchronously by artisan
     * and should create the file correspondent to the
     * key, for gitosis use
     */
    public function create_key_file($value='')
    {
        // Update repository status
        $result = GitOperations::refresh_keys( $this->owner );
        if ( $result )
        {
            // Mark as fine
            $this->status = "fine";
        }
        else
        {
            // Mask as crashed and send an e-mail
            // to admin. Cause this is serious!
            $this->status = "error";

            $crash_report = new Mail( array(
                'user_id' => User::min('id'),
                'subject' => 'Crash Report',
                'message' => View::make('emails.key_error')
                    ->with('key', $this)->render(),
            ));

            $crash_report->save();
        }

        return $this->save();
    }
}
