@layout('master')

@section('page')
    <div class="grid_9">
        <div class="whiteboard">
            <h1>Dashboard</h1>

            <h3>Repositories</h3>
            @foreach($repositories as $repo)
                <a class="repo" href='{{ URL::to( 'repositories/show/'.$repo->id ) }}'>
                    <i class = "status_icon {{ $repo->status_string() }}"></i>
                    <hr>
                    <span class="owner">
                        {{ $user->username }} /
                    </span>
                    <span class="name">
                        {{ $repo->name }}
                    </span>
                </a>
            @endforeach

            <a class="repo new" href='{{ URL::to( 'repositories/new/' ) }}'>
                <span>Start new repository</span>
            </a>

            <span class="clear"></span>

            <h2>News feed</h2>
            <p>You have no news</p>

        </div>
    </div>

    <div class="grid_3">
        <div class="blackboard">
            <h2>{{ $user->username }}</h2>
            <span class="avatar">
                {{ Gravitas\API::image($user->email, 50) }}
            </span>

            <h3>SSH Keys</h3>
            <p>
                @forelse( $user->keys as $key )
                    {{ HTML::link('keys/edit/'.$key->id, $key->name, array('class' => 'ssh_key' )) }}
                    <br/>
                @empty
                    You have no keys
                    <br/>
                @endforelse
                {{ HTML::link('keys/new', 'New...'); }}
            </p>
        </div>
    </div>
@endsection
