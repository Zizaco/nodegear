@layout('master')

@section('page')
    <div class="grid_12">
        <h1><small>Welcome to</small> NodeGear</h1>
    </div>

    <div class="grid_7">
        <h3>What is this</h3>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

        <h3>How it works</h3>
        <p>
            Esse cillum dolore eu fugiat nulla pariatur.
        </p>

        
    </div>

    <div class="grid_5">
        <div class="blackboard">
            <h2>About</h2>
            <blockquote>
                Do whatever you feel like bro!
            </blockquote>
            <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione.
            </p>
        </div>
    </div>
@endsection
