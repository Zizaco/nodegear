@layout('master')

@section('page')
    <div class="grid_12">
        <div class="blackboard fancy_box signup">

            <i class="status_icon alert"></i>

            <h2>Confirmation required</h2>

            <p>An confirmation link has been sent to the email address you supplied.</p>

            <p>Please check your email for a message confirming your registration.</p>

            {{ HTML::link('/','Return to home') }}
        </div>
    </div>
@endsection
