@layout('master')

@section('page')
    <div class="grid_12">
        <div class="blackboard fancy_box signup">

            <i class="status_icon success"></i>

            <h2>Account confirmed</h2>

            <p>
                Go ahead and
                {{ HTML::link('/users/login','Login') }}
            </p>

            {{ HTML::link('/','Return to home') }}
        </div>
    </div>
@endsection
