@layout('master')

@section('page')
    <div class="grid_12">
        <div class="blackboard fancy_box signup">
            <h3>Signup</h3>
            <p>Signup for a free NodeGear account</p>

            {{ Form::open('users/signup', 'POST', null, null, User::$rules) }}

                {{ Form::label('username', 'Username') }}
                {{ Form::text('username', Input::old('username'), array('placeholder' => 'Username')) }}

                {{ raw(Form::label('email', 'E-mail <small>Confirmation required</small>')) }}
                {{ Form::text('email', Input::old('email'), array('placeholder' => 'Email')) }}

                {{ Form::label('password', 'Password') }}
                {{ Form::password('password', array('placeholder' => 'Password')) }}

                @foreach($errors->all() as $error)
                    <p class='error'>{{ $error }}</p>
                @endforeach

                {{ Form::submit('Create new account', array('class' => 'green')) }}

            {{ Form::close() }}
        </div>
    </div>
@endsection
