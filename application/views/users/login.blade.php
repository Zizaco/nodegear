@layout('master')

@section('page')
    <div class="grid_12">
        <div class="blackboard fancy_box signup">
            <h2>Login</h2>

            {{ Form::open('users/login', 'POST') }}

                {{ Form::label('email', 'E-mail') }}
                {{ Form::text('email', '', array('placeholder' => 'Email')) }}

                {{ Form::label('password', 'Password') }}
                {{ Form::password('password', array('placeholder' => 'Password')) }}

                @if( Session::has('error') )
                    <p class='error'>{{ Session::get('error'); }}</p>
                @endif

                {{ Form::submit('Sign In') }}

            {{ Form::close() }}
        </div>
    </div>
@endsection
