@layout('master')

@section('page')
    <div class="grid_12">
        <div class="blackboard fancy_box signup">

            <i class="status_icon error"></i>

            <h1><small>Error</small> 404</h1>
            <p>We cannot find an object at the address that you specified.</p>

            {{ HTML::link('/','Return to home') }}
        </div>
    </div>
@endsection
