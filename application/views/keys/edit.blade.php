@layout('master')

@section('page')
   <div class="grid_12">
        <div class="blackboard fancy_box signup">
            <h2>Editing SSH Key <small>{{ $key->name }}</small></h2>

            {{ Form::open( 'keys/update/'.$key->id, 'PUT' ) }}

                {{ raw(Form::label( 'name', 'Key Name <small>Ex: "home", "my_notebook", "office_pc"</small>' )) }}
                {{ Form::text('name', (Input::old('name') != '') ? Input::old('name') : $key->name ) }}

                {{ Form::label('owner', 'Owner') }}
                {{ Form::text('owner', $user->username, array('disabled')) }}

                {{ raw(Form::label('value', 'Key <small>Paste the content of your .pub file</small>')) }}
                {{ Form::textarea('value', (Input::old('value') != '') ? Input::old('value') : $key->value, array('placeholder' => 'ssh-rsa XXXXXXX...')) }}

                @foreach($errors->all() as $error)
                    <p class='error'>{{ $error }}</p>
                @endforeach

                {{ Form::submit('Update key') }}

            {{ Form::close() }}
        </div>
    </div>
@endsection
