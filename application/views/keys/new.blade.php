@layout('master')

@section('page')
   <div class="grid_12">
        <div class="blackboard fancy_box signup">
            <h1>Add new SSH Key</h1>

            {{ Form::open( 'keys/create', 'POST' ) }}

                {{ raw(Form::label( 'name', 'Key Name <small>Ex: "home", "my_notebook", "office_pc"</small>' )) }}
                {{ Form::text('name', Input::old('name')) }}

                {{ Form::label('owner', 'Owner') }}
                {{ Form::text('owner', $user->username, array('disabled')) }}

                {{ raw(Form::label('value', 'Key <small>Paste the content of your .pub file</small>')) }}
                {{ Form::textarea('value', Input::old('value'), array('placeholder' => 'ssh-rsa XXXXXXX...')) }}

                @foreach($errors->all() as $error)
                    <p class='error'>{{ $error }}</p>
                @endforeach

                {{ Form::submit('Add new key') }}

            {{ Form::close() }}
        </div>
    </div>
@endsection
