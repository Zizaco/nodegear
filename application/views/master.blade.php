<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <title>NodeGear</title>

  <!-- Application root. -->
  <base href="/">

  <!-- Application assets. -->
  {{ Asset::styles() }}
  {{ Asset::scripts() }}
</head>

<body>
  <!-- Main container. -->
  <div id="wrap">
    <!-- main content (everything!) -->
    <div id="content">
      <div id="top">
        <div class="container_12">
          <div class="grid_8">
            <span class="logo_at_header">
              NodeGear
            </span>
          </div>

          <div class="grid_4">
            <div id="navigation">
              @if( Auth::check() )
                {{ HTML::link('dashboard', 'Dashboard'); }}
                {{ HTML::link('users/show/', 'Account'); }}
                {{ HTML::link('users/logout', 'Logout'); }}
              @else
                {{ HTML::link('/', 'Home'); }}
                {{ HTML::link('about', 'About'); }}
                {{ HTML::link('users/signup', 'Sign up'); }}
                {{ HTML::link('users/login', 'Login'); }}
              @endif
              
            </div>
          </div>
        </div>
      </div>

      <div class="container_12">
        <div id="page">
          @yield('page')
        </div>
      </div>
    </div>
  </div>
  <!-- the sticky footer -->
  <div id="footer">
    <div class="container_12">
      <div class="grid_12">
        This project is currently maintained by
        <a href="http://zizaco.net">Zizaco</a>
      </div>
    </div>
  </div>

</body>
</html>
