@layout('master')

@section('page')
   <div class="grid_12">
        <div class="blackboard fancy_box signup">
            <h1>New repository</h1>

            {{ Form::open('repositories/create', 'POST') }}

                {{ Form::label('name', 'Repository name') }}
                {{ Form::text('name', Input::old('name'), array('placeholder' => 'Repository name')) }}

                {{ Form::label('owner', 'Owner') }}
                {{ Form::text('owner', $user->username, array('disabled')) }}

                {{ Form::label('description', 'Description') }}
                {{ Form::textarea('description', Input::old('description'), array('placeholder' => 'A software project that have the objective of ...')) }}

                @foreach($errors->all() as $error)
                    <p class='error'>{{ $error }}</p>
                @endforeach

                {{ Form::submit('Create new repository') }}

            {{ Form::close() }}
        </div>
    </div>
@endsection
