@layout('master')

@section('page')
    <div class="grid_12">
        <div class="whiteboard">

            <div class="repo-info">
                <small>Size: {{ $repo->size() }}</small>
            </div>

            <h1>
                <small>{{ $repo->owner->username }} / </small>
                {{ $repo->name }}
            </h1>

            <p>
                Clone this repository:
                <span class='ssh_address'>
                    $ git clone
                    git@nodegear.org:{{ $repo->owner->username }}/{{ $repo->name }}
                </span>
            </p>

            <hr/>

            @if( $repo->description )
                <p>{{ $repo->description }}</p>
            @endif
        </div>
    </div>
@endsection
