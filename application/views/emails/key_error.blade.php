<h1>NodeGear Crash Report</h1>

<p>Hey Admin,</p>

<p>
    The ssh_key for {{ $key->owner->username }} called {{ $key->name }} has crashed while being created.
</p>
<p>
    Please check if the keys directory is valid. Also check if the gitosis.conf file in gitosis bare repository is correct.
</p>

<p>Regards<br>NodeGear Team</p>
