<h1>NodeGear Crash Report</h1>

<p>Hey Admin,</p>

<p>
    The repository '{{ $repository->owner->username }}/{{ $repository->name }}' has crashed while being created.
</p>
<p>
    Please check if the repository directory is valid and if there is a bare repository there. Also check if the gitosis.conf file in gitosis bare repository is correct.
</p>

<p>Regards<br>NodeGear Team</p>
