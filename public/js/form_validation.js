$(document).ready(function() {
    $('form[data-validate]').validator({
      events                  : 'submit',
      selector                : 'input[type!=submit], select, textarea',
      preventDefaultIfInvalid : true,
      callback                : function( elem, valid ) {
          if ( ! valid ) {
              $( elem ).addClass( 'error' );
          }
      }
    });
    $('form[data-validate] input[data-validations]').validator({
      events   : 'blur change',
      callback : function( elem, valid ) {
          if ( valid ) {
              $( elem ).removeClass( 'error' );
          }
          else {
              $( elem ).addClass( 'error' );
          }
      }
    });
});
