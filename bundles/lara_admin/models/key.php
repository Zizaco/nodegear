<?php namespace Admin; 
 class Key extends Appmodel{ 

    /**
     * Belongs to relation to Lara_admin
     */
    public $belongModels= array(
        "owner_id"=>array("model"=>"User", "column"=>"username", "showon"=>"both", "alternative_text" => "-")
    );

    /**
     * Lara_admin settings for model
     */
 	public static $table ='keys';  
 	public $index= array('owner_id','name');  
 	public $new=array();  
 	public $edit= array(
        'name',
        'owner_id',
        'value'=>array('type'=>'textarea'),
        'created_at'=>array('class'=>'datetime'),
    );  
 	public $show= array();  
 	public $rules= array();  
 }
