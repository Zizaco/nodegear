<?php namespace Admin; 
 class Repository extends Appmodel{ 

    /**
     * Belongs to relation to Lara_admin
     */
    public $belongModels= array(
        "owner_id"=>array("model"=>"User", "column"=>"username", "showon"=>"both", "alternative_text" => "-")
    );

    /**
     * Lara_admin settings for model
     */
 	public static $table ='repositories';  
 	public $index= array('name','owner_id','status','created_at');  
 	public $new=array();  
 	public $edit= array(
        'name',
        'owner_id',
        'status'=>array('type'=>'radioButton',
            'radioButtonOption'=>array(
                'options'=>array(
                    'todo'=>'todo',
                    'fine'=>'fine',
                    'error'=>'error'
                )
            )
        ),
        'description'=>array('type'=>'textarea'),
        'created_at'=>array('class'=>'datetime'),
    );  
 	public $show= array();  
 	public $rules= array();  
 }
