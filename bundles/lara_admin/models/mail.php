<?php namespace Admin; 
 class Mail extends Appmodel{ 

    /**
     * Belongs to relation to Lara_admin
     */
    public $belongModels= array(
        "user_id"=>array("model"=>"User", "column"=>"username", "showon"=>"both", "alternative_text" => "-")
    );

    /**
     * Lara_admin settings for model
     */
 	public static $table ='mails';  
 	public $index= array('user_id','subject','sent','created_at');  
 	public $new=array();  
 	public $edit= array(
        'user_id',
        'subject',
        'sent'=>array('type'=>'radioButton',
            'radioButtonOption'=>array(
                'options'=>array('1'=>'yes', '0'=>'no')
            )
        ),
        'message'=>array('type'=>'textarea'),
        'created_at'=>array('class'=>'datetime'),
    );  
 	public $show= array();  
 	public $rules= array();  
 }
