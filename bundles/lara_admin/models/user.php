<?php namespace Admin; 
 class User extends Appmodel{ 

    /**
     * Lara_admin settings for model
     */
 	public static $table ='users';  
 	public $index= array('username','email','active','created_at');  
 	public $new=array();  
 	public $edit= array(
        'username',
        'email',
        'active'=>array('type'=>'radioButton',
            'radioButtonOption'=>array(
                'options'=>array('1'=>'yes', '0'=>'no')
                )
            ),
        'password'=>array('type'=>'password'),
        'created_at'=>array('class'=>'datetime'),
    );  
 	public $show= array();  
 	public $rules= array();  
 }
