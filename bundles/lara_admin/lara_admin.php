<?php

class LaraAdmin
{

	public static function make()
	{
		Config::set(
			'laraAdmin.models',
			array(
				'User',
				'Repository',
				'Mail',
				'Key'
			)
		);

		Config::set(
			'laraAdmin.title',
			"<a href='/lara_admin'>".
				"<img src='".URL::base()."/img/logo_small.png' height='20px' alt='NodeGear'>".
			"</a>"
		);
	}

}
